import shuffle from "shuffle-array";

function removeFirstChar(str) {
  return str.slice(1);
}

export function sortSpeakers({
  data,
  setSpeakers,
  setAllSpeakers,
  setAllShedules,
}) {
  const allSpeaker = data.allSpeaker.nodes
    .filter((speaker) => {
      const firstChar = speaker.name.charAt(0);
      return firstChar === "!" || (firstChar !== "?" && firstChar !== "-");
    })
    .map((speaker) => ({ ...speaker, name: removeFirstChar(speaker.name) }));

  const withPictures = allSpeaker.filter((speaker) => speaker.picture);
  setAllSpeakers(withPictures);

  setSpeakers(shuffle.pick(withPictures, { picks: 6 }));

  const allSchedules = data.allSchedules.nodes
    .filter((node) => node.start_time)
    .map((node) => {
      const speaker = data.allSpeaker.nodes.find(
        (speakerNode) => speakerNode.id === node.speaker_id
      );
      return {
        ...node,
        speaker:
          speaker !== undefined
            ? { ...speaker, name: removeFirstChar(speaker.name) }
            : speaker,
      };
    });

  setAllShedules(allSchedules);
}
