import React, { useEffect, useRef, useState } from "react";
import {
  ArrowRight as ArrowRightIcon,
  Instagram as InstaIcon,
  Mail as EmailIcon,
} from "react-feather";
import { Helmet } from "react-helmet";
import { FaWhatsapp } from "react-icons/fa";
import logoImg from "./assets/logo.png";
import spartialLogoImg from "./assets/spartial.png";
import youtubeLogoImg from "./assets/youtube-logo.png";
import DivingManIlustration from "./components/DivingManIlustration";
import HelmetContent from "./components/HelmetContent";
import LinkButton from "./components/LinkButton";
import NavBar from "./components/NavBar";
import ScheduleList from "./components/ScheduleList";
import SpeakerListPreview from "./components/SpeakerListPreview";
import SpeakerModal from "./components/SpeakerModal";
import Sponsors from "./components/Sponsors";
import Price from "./components/Price";
import kitCongressista from "./assets/kit.png";
import saville from "./assets/saville.jpeg";
import iago from "./assets/iago.jpg";
import daniel from "./assets/daniel.jpg";
import pedro from "./assets/pedro.jpeg";
import gaban from "./assets/gaban.jpg";
import andre from "./assets/andre.jpg";
import ana from "./assets/ana.jpeg";
import hellen from "./assets/hellen.jpg";
import joselito from "./assets/joselito.jpg";
import itala from "./assets/itala.JPG";
import marcela from "./assets/cela.jpg";
import caio from "./assets/caio.jpeg";
import maya from "./assets/maya.jpg";
import kauane from "./assets/kauane.png";
import rafaela from "./assets/rafaela.jpeg";
import david from "./assets/david.jpeg"
import fernanda from "./assets/fernanda.jpg"
import soterotropicanas from "./assets/soterotropicanas.jpeg";
import manuela from "./assets/manuela.jpeg"
import reinaldo from "./assets/reinaldo.jpeg"
import tiago from "./assets/tiago.jpg"
import "./reset.css";
import Container, {
  Card,
  ContentContainer,
  FooterSection,
  Main,
  PageContainer,
  Summary,
} from "./styles";
import Partners from "./components/Partners";

const speakersList = [
  {
    id: 1,
    name: "Iago Santos",
    description:
      "Iago Santos, sou um aprendedor e empreendedor, Designer Gráfico em constante aprendizado, Especialista em Inovação Digital pela Digital Innovation One. Mentor e consultor em comunicação e negócio, idealizador e CEO do TrazFavela Delivery (@trazfavela) e Coordenador de Inovação da AJE Bahia (Associação de Jovens Empreendedores da Bahia).",
    picture: iago,
  },
  {
    id: 2,
    name: "Pedro Rio",
    description:
      "Formando na UFBA, é co-fundador e CEO da Clarke Energia. Selecionado para a lista da Forbes Under 30 de 2020, foi Presidente da Confederação Brasileira de Empresas Juniores (Brasil Júnior) em 2016, trabalhou na Ambev em 2017 e em 2018 foi uma das lideranças da campanha do Felipe Rigoni (primeiro deputado federal cego da história).",
    picture: pedro,
  },
  {
    id: 3,
    name: "Saville Alves",
    description:
      "Formada pela UFBA em comunicação social, integrante do grupo de pesquisa GPS adm-UFBA, tenho atuado para que os negócios sejam expressões da necessidades sociais, gerando inclusão e equilíbrio. Minha militância iniciou a partir das experiências na universidade pública, quando atuei como uma das principais lideranças da Federação Baiana de Empresas Juniores e posteriormente como diretora da Associação de Jovens Empresários da Bahia. Conheci de perto o mercado, trabalhando em empresas de capital aberto, como a Oi S.A. e Braskem, e também encarei de perto os paradigmas sociais que vivem os mais vulneráveis, como coordenadora de captação de recursos da ONGs TETO e ARCAH. Essa pluralidade de percepções me levou a co-fundar a SOLOS, startup que implementa soluções para a cadeia circular dos resíduos.",
    picture: saville,
  },
  {
    id: 4,
    name: "André Fraga",
    description:
      "Engenheiro Ambiental, pós-graduado em Gerenciamento de Projetos pela Fundação Getúlio Vargas (FGV), está no doutorado pela Faculdade de Medicina da Universidade de São Paulo (FMUSP) é Vice-Presidente da Sociedade Brasileira de Arborização Urbana e foi ex-Secretário Municipal de Sustentabilidade, Inovação e Resiliência de Salvador. Integra a Rede de Ação Política pela Sustentabilidade (RAPS) e do RenovaBR Cidades, onde foi professor da disciplina Políticas Públicas Municipais para a Sustentabilidade. Vereador de Salvador pelo Partido Verde e presidente da Comissão Especial de Emergência Climática e Inovação da Câmara Municipal de Salvador",
    picture: andre,
  },
  {
    id: 5,
    name: "Luis Gaban",
    description:
      "Advogado. Diretor de Inovação da Prefeitura de Salvador. Secretário executivo do conselho municipal de inovação. Membro do Comitê gestor do Fundo Municipal de Inovação. Secretário executivo do Comitê do Programa Inova Salvador. Membro da câmara de inovação - CIT/ FECOMÉRCIO BA",
    picture: gaban,
  },
  {
    id: 6,
    name: "Rafaela Rodrigues",
    description:
      "Especialista em Gestão de Negócios e Projetos Especiais, Rafaela Rodrigues é Gerente de Inovação e Empreendedorismo do Parque Tecnológico da Bahia, Líder Inovativa pela Bahia e Líder do Comitê de Empreendedorismo do Grupo Mulheres do Brasil – Núcleo Salvador. Atua a mais de 7 anos pelo desenvolvimento do ecossistema de empreendedorismo e inovação da Bahia. Entre outras iniciativas, atuou na elaboração da Lei que instituiu a Política Municipal de Inovação em Salvador e do projeto Colabore – Centro de Inovação de Impacto de Salvador. É também mentora de negócios e já colaborou com diversos programas e eventos, a exemplo do InovAtiva, Startup Weekend, do Programa MAE - Mulheres-anjo empreendedoras da Unifacs e maratona de inovação mundial Nasa Space Apps Challenge.",
    picture: rafaela,
  },
  {
    id: 7,
    name: "Daniel Neiva",
    description:
      "Character Design, Ilustrador e Arte Educador, Técnico em Programação de jogos digitais pelo Senai, Graduando em Jogos Digitais/ IFBA. CEO da Aimo Tech, startup de tecnologia educacional fundada em 2018, focada em oficinas artístico-científicas em cultura maker para estudantes da educação básica. @sequeladus",
    picture: daniel,
  },
  {
    id: 8,
    name: "Ana Cristina",
    description:
      "Mãe de Erick, Ana é Administradora formada pela Universidade Salvador, com mais de 10 anos de experiência em processos administrativos. Nesse período, esteve em empresas como Promédica, Indústria Bahiamido, Fisk e na própria Universidade Salvador, em cargos de Gestão, Supervisão Administrativa e Comercial, Secretaria Executiva, Relacionamento, entre outras atividades. Atualmente, responde pela Gerência de Operações da Vale do Dendê, liderando os Projetos de Aceleração voltados para startups de afroempreendedores com base periférica.",
    picture: ana,
  },
  {
    id: 9,
    name: "Hellen Nzinga",
    description:
      "Hellen Nzinga é gestora de projetos sustentáveis. Graduada em comunicação pela Universidade Católica de Salvador, Hellen lecionou nos cursos de comunicação e empreendedorismo do programa Trilha e também é certificada em Inovação Política pela Asuntos Del Sur. Premiada por projetos de educação e tecnologia, foi reconhecida pelo fundo internacional Baobá e pela Mckinsey & Co como uma das lideranças femininas mais influentes do Brasil de 2021. Hoje Hellen é co-fundadora e executiva na EcoCiclo, marca que desenvolveu o absorvente brasileiro 100% biodegradável, e que expandiu seu impacto através de sua plataforma que investe no empreendedorismo sustentável, geração de renda e educação de mulheres.",
    picture: hellen,
  },
  {
    id: 10,
    name: "Joselito Crispim",
    description:
      "Pai, fundador do Grupo Cultural Bagunçaço, padeiro, ex estudante de direito, cineasta, escritor, educador viajado pelo mundo, de onde se sente parte ativa, hoje, depois de muita resistência cedeu aos pedidos de inúmeros amigos está a frente da Presidência Colegiada do Frente Favela Brasil- Bahia.",
    picture: joselito,
  },
  {
    id: 11,
    name: "Itala Herta",
    description:
      "Empreendedora Social e Fundadora da DIVER.SSA. Consultora e empreendedora social graduada em Comunicação Social com ênfase em Relações Públicas pela UNIFACS/LAUREATE. Atua há mais de 15 anos com projetos interculturais de inovação social, economia criativa, sustentabilidade & responsabilidade social em instituições públicas e privadas do Brasil.",
    picture: itala,
  },
  {
    id: 12,
    name: "Marcela Prates",
    description:
      "Head of People and Culture da Konsi e Conselheira Empresa JR. ADM UFBA .",
    picture: marcela,
  },
  {
    id: 13,
    name: "Caio Soares",
    description:
      "Head of Business Development da Konsi, Conselheiro NEJ Salvador e Co-fundador OutBox Law.",
    picture: caio,
  },
  {
    id: 14,
    name: "David Pires",
    description:
      "Paulistano-Baiano empreendedor. Comecei na área de tecnologia aos 19 anos, aprendendo a programar sozinho. Fundei minha primeira empresa, a Cubos ( cubos.io ), em 2013, um grupo econômico que tem como principais atividades uma software house e um venture studio, empregando mais de 100 pessoas diretamente e mais de 300 por meio de suas investidas. Hoje contamos com 8 empresas no grupo. Destaque especial para a Zigpay ( zigpay.com.br ), empresa que fundamos em 2016 , na qual atuo diariamente como diretor de produtos, buscando criar a melhor experiência de consumo no entretenimento não redor do mundo. A Zigpay fundiu em 2021 com a NetPDV, se tornando a maior empresa de pagamentos no mercado de eventos no mundo.",
    picture: david,
  },
  {
    id: 15,
    name: "Fernanda Lavínia",
    description:
      "Tirar coisas do papel, concretizar ideias, formar times e levar soluções ao próximo nível. Minha trajetória é composta pela criação de coisas do zero: ainda na Universidade Federal da Bahia, liderei a criação do Núcleo de Empresas Juniores de Salvador, unindo estudantes de 49 instituições de ensino superior na cidade. Na Intera, já fiz de tudo um pouco: criei a área de Sucesso do Talento, dei os primeiros passos na criação e formei o time da área de Growth Hacking, unindo pessoas de diversas áreas em torno do mesmo ideal: criar uma operação escalável e focada em resultados. Em meio à pandemia, estruturei e toquei o funil de vendas de novos produtos digitais, focados em treinamento e desenvolvimento de pessoas. Hoje lidero um dos projetos vitais para o crescimento do serviço da Intera, que é a Academia de Talent Hackers, nosso processo de seleção e formação de Tech Recruiters. Fui responsável pela construção desse programa humanizado e escalável que vem sendo otimizado e aperfeiçoado a cada edição. Apaixonada por Engenharia porque vejo ela muito além dos cálculos: é a forma mais data driven e focada em resultados de criar soluções que impactam na vida de pessoas e tornam seu dia a a dia cada vez mais fácil.",
    picture: fernanda,
  },
  {
    id: 16,
    name: "Maya Fernandes",
    description:
      "Maya Fernandes é cantora, compositora e produtora cultural soteropolitana. A artista desenvolve seu projeto autoral baseado na pesquisa de ritmos afro baianos em consonância com outras linguagens musicais, desde 2018, quando teve seu single Melanina premiado no Festival MUSA. Em 2020, o pagode Melanina teve a estréia de seu clipe vinculado ao YouTube do TCA. Já em 2021, a artista lançou seu segundo single, o samba reggae, Única que teve apoio do Prêmio Jorge Portugal, para lançamento de seu videoclipe. Como produtora, atualmente a mesma atua na Revista Fraude e também é integrante do projeto NEOJIBA.",
    picture: maya,
  },
  {
    id: 17,
    name: "Kauane Messias",
    description:
      "Oi gente, eu sou Kauane Messias! Cantora, compositora e bailarina! Há um tempo atrás eu estava imersa no mundo do teatro musical! Porém hoje em dia tenho foco em minha carreira solo como artista independente ✨ Vocês podem escutar minhas músicas autorais no Spotify! 🎶 Também tenho um canal no YouTube onde posto músicas e converso com vocês 🎶🎥 Animada para ficar mais pertinho de vocês no dia 16 😘 beijos e até lá!",
    picture: kauane,
  },
  {
    id: 18,
    name: "Soterotropicanas",
    description:
      "Somos Ana Luísa, Cibele, Julia e Renata! Amigas-estudantes de Psicologia, na UFBA! Nos conhecemos na melhor cidade da América do Sul - Salvador! Somos arteiras e nosso encontro é mesmo bastante permeado pela paixão pelas artes; gostamos de brincar de sermos dançarinas, poetizas, instrumentistas e cantoras! Na nossa relação com a música cabe muita admiração pelo o que é feito na Bahia! Temos muito orgulho do nosso samba, da música popular, tradicional, reggae... É um tantinho de tudo isso que queremos levar para vocês! Dendê, corpo, brincadeira e coração! Esse é o nosso encontro.",
    picture: soterotropicanas,
  },
  {
    id: 19,
    name: "Manuela Maciel",
    description:
      "Advogada formada pela UFBA, pós graduanda em Proteção de Dados e Privacidade pelo IDP e certificada na área pelo Instituto Data Privacy Brasil. Legal analyst e DPO no Escavador, e sócia do núcleo de PPD do Chezzi Advogados.",
    picture: manuela,
  },
  {
    id: 20,
    name: "Reinaldo Heck",
    description:
      "Apaixonado por negociação, tecnologia, música e pessoas. Pós Júnior da Ação Júnior (FEJESC), com mais de 7 anos atuando em operações comerciais, já vendeu diretamente mais de 1 milhão de reais, liderou uma equipe com conversão de vendas acima de 40% (Conpass) e criou uma operação comercial do zero (Airbox). Em julho do ano passado, decidiu fundar a Impulse para ajudar empresas a vender mais e crescer de uma maneira sustentável. De lá pra cá, já ajudou mais de 70 empresas juniores a venderem mais de 5 milhões de reais.",
    picture: reinaldo,
  },
  {
    id: 21,
    name: "Tiago Rocha",
    description:
      "Graduado em Administração na FASB e com MBA em Logistica empresarial pela FAFICA. Em 2011 inicia na Ambev como Analista de Distribuição (Logistica), em 2014 como Supervisor de armazém, em 2016 como supervisor de Vendas e em 2020 (atual) Supervisor de Nivel de Serviço. É #AlémDosRotulos: Soteropolitano,criado no oeste da Bahia, casado com Gabi (Pernambucana), pai de Laura e Lucas. Gosto de jogar futebol, pescar,fazer churrasco e de me reunir com a família e amigos.",
    picture: tiago,
  },
];

function App() {

  const [speakers, setSpeakers] = useState();
  const [allSpeakers, setAllSpeakers] = useState([]);
  const [allShedules, setAllShedules] = useState([]);
  
  const [currentSpeaker, setCurrentSpeaker] = useState();
  
  useEffect(() => {
    setAllSpeakers(speakersList);
    setSpeakers(speakersList);
  }, []);
  
  const [isChrome, setIsChrome] = useState();
  useEffect(() => {
    if (typeof window !== "undefined") {
      setIsChrome(window.chrome);
    }
  }, []);
  
  const scrollToRef = (ref) => window.scrollTo(0, ref.current.offsetTop - 120);
  const refContato = useRef(null);
  const refProposta = useRef(null);
  const executeScroll = (ref) => scrollToRef(ref);
  

  return (
    <>
      <Helmet defer={false}>
        <HelmetContent />
      </Helmet>
      <Container>
        <PageContainer>
          <a
            href="https://api.whatsapp.com/send?phone=5571993429897&text=Ol%C3%A1!%20Gostaria%20de%20uma%20ajuda%20com%20o%20ESEJ%2021"
            className="float"
            target="_blank"
          >
            <FaWhatsapp />
          </a>
          <DivingManIlustration enableSvgFilter={!isChrome} />
          <ContentContainer>
            <NavBar
              options={[
                { label: "Início", href: "#" },
                {
                  label: "Proposta",
                  onClick: () => executeScroll(refProposta),
                },
                {
                  label: "Contato",
                  onClick: () => executeScroll(refContato),
                },
                {
                  label: "Inscreva-se",
                  href: "https://beacons.page/nejsalvador",
                  target: "_blank",
                },
              ]}
            />
            <Main>
              <SpeakerModal
                currentSpeaker={currentSpeaker}
                setCurrentSpeaker={setCurrentSpeaker}
              />
              <Summary className="summary">
                <img
                  src={logoImg}
                  alt="logomarca do ESEJ 2021"
                  className="logo"
                />
                <LinkButton
                  href="https://beacons.page/nejsalvador"
                  target="_blank"
                >
                  INSCREVA-SE JÁ
                  <ArrowRightIcon color="#000" size={14} />
                </LinkButton>
              </Summary>
              <section id="proposta" ref={refProposta} className="about">
                <p className="intro">
                  O ESEJ será o principal evento de uma jornada conjunta de todos os eventos do ano que terão um tema norteador: O FUTURO. Partindo dessa premissa, o evento terá como temática principal: <b>"O futuro é agora".</b> Trazendo o marco de resultados do ano como ponto no qual a rede Sotero e Metropolitana comemora o alcance das suas metas.
                </p>
                <p>
                  Queremos desmistificar a ideia de futuro que muitas vezes está associada a algo distante da nossa realidade. Trazer para a pauta o fato de que esse futuro já existe, está cada vez mais presente e qual o papel que a nossa capital, região metropolitana e como o MEJ se insere nesse ecossistema.
                </p>
              </section>
              {/* AREA DOS PALESTRANTES */}
              <section className="section">
                <h1>ALGUNS DOS CONFIRMADOS ATÉ O MOMENTO</h1>
                <h2>Vai ficar fora dessa?</h2>

                <SpeakerListPreview
                  speakers={speakers}
                  allSpeakers={allSpeakers}
                  setCurrentSpeaker={setCurrentSpeaker}
                />
                <ScheduleList shedules={allShedules} />
              </section>
              <section className="section">
                <h1>O FUTURO</h1>
                <h2>O futuro é agora</h2>
                <p>Quando pensamos no futuro muitas vezes pensamos em filmes de ficção científica, carros voadores e tecnologia. Apesar desses elementos fazerem parte do futuro, existem muitas outras questões latentes. Queremos trazer essas questões a tona e entender: <b>quão presente já é o futuro?</b></p>
                <p>
                Teremos como perspectiva pilares para o evento: <b>tecnologia, sustentabilidade, empoderamento social e protagonismo de Salvador enquanto capital de inovação.</b> Justamente partindo das necessidades e saídas que queremos do mesmo, ou seja, ter uma rede mais conectada, comprometida com resultados, disposta a gerar mudança por meio da vivência empresarial e consciente dos problemas da atualidade. 
                </p>
                <iframe
                  className="yt-live"
                  src="https://www.youtube.com/embed/PeJWCX1jGZc"
                  frameborder="0"
                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                  allowfullscreen
                ></iframe>
                <br/>
                <br/>
                <p>
                  Pensando também na imersão dos participantes do nosso evento
                  preparamos uma Lojinha especial para que possam se
                  sentir pertinho do movimento, mesmo distantes. Queremos poder
                  ver vocês <b>vestindo as camisas do movimento</b>, levando no
                  peito nossos valores mostrando qual grito reverbera de farol a farol!{" "}
                  <a href="https://esej21.strelo.com.br/" target="_blank">
                    Garanta já o seu.
                  </a>
                </p>
                <img
                  className="kit"
                  src={kitCongressista}
                  alt="Camisa preta com mapa da região metropolitana de Salvador e o hino do NEJ Salvador, camisa azul com a frase 'Meu grito reverbera', molesquine, bottom e boné com a logo do NEJ Salvador."
                />
              </section>
              <section className="section">
                <h1>INGRESSO</h1>
                <h2>Garanta já sua passagem para o futuro</h2>
                <p>Os nossos ingressos já estão disponíveis e atualmente estamos no <b>3º Lote</b>, temos 3 modalidades de pagamento: <b>PIX, Boleto Bancário e Cartão de Crédito.</b></p>
                <p>
                  Para realizar a compra coletiva com desconto você precisa adquirir no mínimo a porcentagem (%) de ingressos equivalente a sua meta de partcipação em eventos. <a href="https://beacons.page/nejsalvador" target="_blank">Garanta já o seu!</a>
                </p>
                <Price />
              </section>  
              <Sponsors />
              <Partners />
              <FooterSection>
                <h1>CONTATO</h1>
                <section ref={refContato} className="section" id="contato">
                  <Card
                    href="https://www.instagram.com/nejsalvador/"
                    target="_blank"
                  >
                    <div className="icon">
                      <InstaIcon color="#fafafa" size={30} />
                    </div>
                    <div className="content">
                      <b>Instagram</b>
                      <span>@nejsalvador</span>
                    </div>
                  </Card>
                  {/* <Card
                    href="https://api.whatsapp.com/send?phone=557188178613&text=Ol%C3%A1!%20Vi%20o%20contato%20no%20site%20da%20semcomp%202020"
                    // target="_blank"
                    target="_blank"
                  >
                    <div className="icon">
                      <WhatsappIcon color="#fafafa" size={30} />
                    </div>
                    <div className="content">
                      <b>Whatsapp</b>
                      <span>+55 (71) 8743-6097</span>
                    </div>
                  </Card> */}
                  <Card
                    href="mailto:conexoesnejsalvador@gmail.com"
                    target="_blank"
                  >
                    <div className="icon">
                      <EmailIcon color="#fafafa" size={30} />
                    </div>
                    <div className="content">
                      <b>Email</b>
                      <span>conexoesnejsalvador</span>
                    </div>
                  </Card>
                </section>
                <section
                  ref={refContato}
                  className="spaces-container"
                  id="contato"
                >
                  <Card
                    target="_blank"
                    href="https://www.youtube.com/channel/UCMf0S6lCYVGSvuYqP5p4C0A"
                    className="spaces column"
                    color="#FF0000"
                  >
                    <img className="youtube-logo" src={youtubeLogoImg} />
                  </Card>
                  <Card
                    target="_blank"
                    href="https://esej21.spatial.chat"
                    className="spaces column"
                    color="#2BD094"
                  >
                    <img className="youtube-logo" src={spartialLogoImg} />
                  </Card>
                </section>
                {/* <iframe
                  width="100%"
                  height="396"
                  // style={{
                  //   marginTop: 180,
                  //   boxSizing: "border-box"
                  //   // paddingTop: 40
                  // }}
                  // frameborder="0"
                  // style="border:0"
                  src="https://www.google.com/maps/embed/v1/place?key=AIzaSyBYNRc-AcUv6CRaATV2_AJ8zcYpvOp7uQU
    &q=Pavilhao+de+Aulas+Reitor+Felipe+Serpas+UFBA+Salvador,Ba+Brasil"
                  allowFullScreen
                ></iframe> */}
              </FooterSection>
            </Main>
          </ContentContainer>
        </PageContainer>
      </Container>
    </>
  );
}

export default App;
