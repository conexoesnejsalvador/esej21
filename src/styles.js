import styled from "styled-components";

export default styled.div`
  background: linear-gradient(to right bottom, #0b71d6, #0092e8, #00add9, #00c2af, #00d17a, #01c672, #00bb6b, #00b063, #008977, #006072, #003854, #021427);
  color: #fafafa;
  margin: 0;
  padding: 0;
  width: 100vw;
  overflow: hidden;
  min-height: 100vh;
  position: relative;
  margin: 0 auto;

  .video {
    /* height: auto; */
    width: 100%;
    position: absolute;
    left: 0;
    top: 0;
    z-index: 0;
    min-width: 100vw;
    height: 56.25vw;

    @media screen and (max-width: 664px) {
      min-height: 374px;
      padding: 0;
    }
  }

  .kit {
    width: 100%;
  }

  .float {
    position: fixed;
    width: 60px;
    height: 60px;
    bottom: 40px;
    right: 40px;
    background-color: #25d366;
    color: #fff;
    border-radius: 50px;
    text-align: center;
    font-size: 30px;
    box-shadow: 2px 2px 3px #999;
    z-index: 10000000000;
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .my-float {
    margin-top: 16px;
  }
`;

export const PageContainer = styled.div`
  color: #fafafa;
  /* display: grid;
  grid-template-columns: 1fr minmax(450px, 1fr) minmax(450px, 1fr) 1fr; */
  margin: 0;
  padding: 0;
  width: 100%;
  max-width: 1500px;
  position: relative;
  overflow-x: visible;
  margin: 0 auto;
  min-height: 1740px;
`;

export const ContentContainer = styled.div`
  .MuiTypography-colorTextSecondary {
    color: rgba(255, 255, 255, 0.6);
  }
  position: relative;
  width: 100%;
  height: 100%;
  min-height: 1740px;
  display: grid;
  grid-template-columns: 1fr 940px 1fr;
  @media screen and (max-width: 940px) {
    grid-template-columns: 0 1fr 0;
  }
  align-content: flex-start;
  /* grid-template-rows: 60px auto-f; */
  overflow: hidden;
  z-index: 300;
`;

export const Main = styled.main`
  grid-column: 2/3;
  display: grid;
  grid-template-columns: 1fr 1fr;
  color: #fafafa;
  box-sizing: border-box;
  font-size: 1.24rem;
  padding: 0 20px;
  @media screen and (max-width: 950px) {
    padding: 0 34px;
  }
  .about {
    margin-top: 100px;
    @media screen and (max-width: 670px) {
      grid-column: 1/3;
    }
  }

  .section {
    margin-top: 80px;
    grid-column: 1/3;

    h1 {
      margin-bottom: 0;
    }

    h2 {
      font-size: 1.2rem;
      margin: 8px 0;
    }

    p {
      margin-top: 0;
    }

    p a {
      text-decoration: underline;
    }
  }

  .card-container {
    display: grid;
    grid-template-columns: 1fr;
    grid-column: 1/2;
    grid-gap: 10px;
    /* h1 {
      grid-column: 1/2;
    } */
    /* padding-bottom: 180px; */

    /* @media screen and (max-width: 1024px) {
      grid-template-columns: repeat(2, 1fr);

      h1 {
        grid-column: 1/2;
      }
    }

    @media screen and (max-width: 687px) {
      grid-template-columns: 1fr;

      h1 {
        grid-column: 1/2;
      }
    } */
  }
`;

export const Card = styled.a`
  width: 100%;
  &.spaces {
    background: ${(p) => (p.color ? p.color : "#AAA")};
  }
  margin-bottom: 12px;
  min-height: ${(p) => (p.minHeight ? `${p.minHeight}` : "120px")};
  box-sizing: border-box;
  padding: ${(p) => (p.padding ? `${p.padding}` : "0 12px")};
  border-radius: 4px;
  box-shadow: 1px 8px 30px rgba(0,0,0,0.3);
  background: #10101d;
  transition: transform 0.3s cubic-bezier(0.68, -0.55, 0.27, 1.55);
  transform: translateY(0px);
  :hover {
    transform: translateY(-18px);
    cursor: pointer;
    text-decoration: none;
  }

  display: grid;
  grid-template-columns: 80px 1fr;
  align-content: center;
  justify-content: center;
  justify-items: center;
  /* align-items: center; */

  .icon {
    box-sizing: border-box;
    padding: 20px;
    width: 100%;
    color: #fafafa;
  }
  .content {
    display: flex;
    flex-direction: column;
    justify-content: center;
    justify-self: flex-start;
  }

  a {
    margin: 4px 0;
    /* font-size: 6rem; */
  }
`;

export const VideoOverlay = styled.div`
  background: linear-gradient(
    183.6deg,
    rgba(0, 0, 0, 0) -65.61%,
    #010102 86.22%
  );
  width: 100vw;
  min-width: 664px;
  /* min-height: 334px; */
  padding-bottom: 56.25%;
  z-index: 100;
  height: 0;
  position: absolute;

  @media screen and (max-width: 664px) {
    min-height: 374px;
    padding: 0;
  }
`;

export const Summary = styled.div`
  grid-column: 1/3;
  margin-top: 140px;
  box-sizing: border-box;
  /* padding: 0 20px; */
  width: 50%;
  display: flex;
  flex-direction: column;
  transition: width 1s ease-in-out;

  align-items: flex-start;
  @media screen and (max-width: 670px) {
    width: 100%;
    align-items: center;
  }
  @media screen and (max-width: 950px) {
    /* align-items: center; */
  }

  img.logo {
    width: 500px;
    min-width: 307px;
    @media screen and (max-width: 670px) {
      width: 70%;
      margin-left: 0;
    }
  }
  p {
    text-align: center;
    margin-top: 4px;
    font-size: 1.4rem;
    font-weight: bold;
  }
`;

export const FooterSection = styled.div`
  display: grid;
  grid-column: 1/3;
  grid-template-columns: 1fr 1fr;
  grid-gap: 30px;
  padding-bottom: 80px;

  iframe,
  .spaces-container {
    // margin-top: 160px;
    box-sizing: border-box;
  }

  .section {
    margin-top: 0;
    grid-column: 1/1;
  }

  // .spaces {
  //   margin-bottom: 12px;
  // }

  @media screen and (max-width: 670px) {
    grid-template-columns: 1fr;
    iframe,
    .spaces {
      margin-top: 0;
    }
  }

  .spaces-container {
    a {
      display: flex;
      &.column {
        flex-direction: column;
        align-items: center;
        span {
          transform: translateY(-4px);
        }
      }
    }
  }

  .spaces img {
    width: auto;
    align-self: center;
  }

  .youtube-logo {
    max-height: 40px;
  }
`;
