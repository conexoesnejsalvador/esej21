import styled from "styled-components";

export const Svg = styled.svg`
  position: absolute;
`;

export const Img = styled.img`
  position: absolute;
  width: 700px;
  /* top: 200px; */
  z-index: 200;
  right: -100px;
  transition: all 1s ease-in-out;
  @media screen and (min-width: 687px) {
     ${(props) =>
       props.enableSvgFilter ? '-webkit-filter: url("#turbulence");' : ""}
    filter: ${(props) =>
      props.enableSvgFilter ? 'url("#turbulence")' : "none"} !important;
  }

  @media screen and (max-width: 750px) {
    opacity: 0.6;
  }

  @media screen and (max-width: 700px) {
    opacity: 0.4;
  }

  @media screen and (max-width: 670px) {
    top: -100px;
    right: -180px;
    opacity: 0.3;
  }
`;
