import React from "react";

import divingImg from "../../assets/diving-man.png";

import { Img, Svg } from "./styles";

function DivingManIlustration({ enableSvgFilter }) {
  return (
    <>
      <Svg>
        <filter id="turbulence" x="0" y="0" width="100%" height="100%">
          <feTurbulence
            height="100%"
            id="sea-filter"
            numOctaves="1"
            seed="3"
            baseFrequency="5"
          ></feTurbulence>
          <feDisplacementMap scale="8" in="SourceGraphic"></feDisplacementMap>
          <animate
            xlinkHref="#sea-filter"
            attributeName="baseFrequency"
            dur="60s"
            keyTimes="0;0.5;1"
            values="0.02 0.06;0.04 0.08;0.02 0.06"
            repeatCount="indefinite"
          />
        </filter>
      </Svg>
      <Img enableSvgFilter={enableSvgFilter} alt="" src={divingImg} />
    </>
  );
}

export default DivingManIlustration;
