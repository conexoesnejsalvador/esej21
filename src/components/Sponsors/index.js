import React from "react";
import ambevLogo from "../../assets/ambev.png";
import streloLogo from "../../assets/strelo-logo.png";
import interaLogo from "../../assets/intera.png";
import escavadorLogo from "../../assets/escavador.svg";
import konsiLogo from "../../assets/konsi.png";
import cubosLogo from "../../assets/cubos.png";
import {
  Container,
  ContentContainer,
  Sponsor,
  SponsorContainer,
} from "./styles";

const sponsors = [
  {
    name: "AMBEV",
    uri: "https://www.ambev.com.br/",
    logo: ambevLogo,
  },
  {
    name: "Strelo",
    uri: "https://www.strelo.com.br/",
    logo: streloLogo,
  },
  {
    name: "Intera",
    uri: "https://byintera.com/",
    logo: interaLogo,
  },
  {
    name: "Escavador",
    uri: "https://www.escavador.com/",
    logo: escavadorLogo,
  },
  {
    name: "Cubos Tecnologia",
    uri: "https://cubos.io/",
    logo: cubosLogo,
  },
  {
    name: "Konsi",
    uri: "https://www.tradeoff.com.br/",
    logo: konsiLogo,
  },
];

function Sponsors() {
  return (
    <Container className="section">
      <h1>PATROCINADORES</h1>

      <ContentContainer>
        {sponsors.map((sponsor) => (
          <SponsorContainer
            href={sponsor.uri}
            target={sponsor.uri !== "#" && "_blank"}
            title={sponsor.name}
          >
            <Sponsor logoSize={sponsor.logoSize} src={sponsor.logo} />
          </SponsorContainer>
        ))}
      </ContentContainer>
    </Container>
  );
}

export default Sponsors;
