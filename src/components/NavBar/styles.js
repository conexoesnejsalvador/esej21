import styled from "styled-components";

export const Container = styled.nav`
  display: grid;
  align-items: center;
  grid-column: 2/3;
  padding: 24px 0;
  width: 100%;
  left: 0;
  position: fixed;
  z-index: 50;
  transition: background 0.3s ease-in-out;
  background: ${(p) => (p.scrollPosition > 60 ? "#10101d" : "transparent")};
  grid-template-columns: 1fr;

  @media screen and (min-width: 1500px) {
    grid-template-columns: 1fr 1500px 1fr;
    menu {
      grid-column: 2/2;
    }
  }

  @media screen and (max-width: 950px) {
    background: ${(p) => (p.open ? "#10101d" : "transparent")};
  }

  menu {
    max-width: 1500px;
    padding: 0 12px 0;
    margin: 0;
    flex-grow: 1;
    display: flex;
    box-sizing: border-box;

    svg {
      min-height: 30px;
      display: none;
      :hover {
        cursor: pointer;
      }
    }

    @media screen and (max-width: 950px) {
      flex-direction: column;
      overflow: hidden;
      max-height: ${(p) => (p.open ? "none" : "30px")};

      svg {
        display: block;
        /* opacity: ${(p) => (p.open ? 0 : 1)}; */
      }

      ul {
        flex-direction: column;
        width: 100%;

        li {
          margin: 8px 0;
        }
      }
    }
  }
  ul {
    list-style: none;
    display: flex;
    justify-content: space-evenly;
    padding: 0;
    margin: 0;
    width: 100%;
  }
  li {
    margin: 0;
    padding: 0;

    a {
      cursor: pointer;
      font-size: 1.17rem;
    }
    :first-child {
      a {
        text-decoration: none;
      }
    }
  }
  a {
    color: #ffffff;
    text-decoration: none;

    /* text-decoration: line-through; */

    :hover {
      text-decoration: underline;
      /* text-decoration: line-through; */
    }
  }
`;
