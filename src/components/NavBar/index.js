import React, { useState } from "react";
import useScrollPosition from "../../utils/useScrollPosition";

import { Container } from "./styles";

import { Menu as MenuIcon, X as CloseIcon } from "react-feather";

function NavBar({ options }) {
  const scrollPosition = useScrollPosition();
  const [open, setOpen] = useState(false);
  return (
    <Container open={open} className="menu" scrollPosition={scrollPosition}>
      <menu>
        {open ? (
          <CloseIcon onClick={() => setOpen(!open)} />
        ) : (
          <MenuIcon onClick={() => setOpen(!open)} />
        )}
        <ul>
          {options.map((option) => (
            <li>
              <a {...option}>{option.label}</a>
            </li>
          ))}
        </ul>
      </menu>
    </Container>
  );
}

export default NavBar;
