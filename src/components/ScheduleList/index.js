import {
  Collapse,
  IconButton,
  List,
  ListItem,
  ListItemIcon,
  ListItemSecondaryAction,
  ListItemText,
  ListSubheader,
  Tooltip,
} from "@material-ui/core";
import { format } from "date-fns";
import React, { useState } from "react";
import {
  Calendar,
  ChevronDown as ArrowDownIcon,
  ChevronRight as ArrowRightIcon,
} from "react-feather";
// import { Container } from './styles';
import "styled-components/macro";
import { useStyles } from "./styles";

function ScheduleList() {
  const classes = useStyles();

  const [currentShedule, setCurrentShedule] = useState();
  const dates = {
    "15": new Date("2021-10-15T00:00:00.000Z"),
    "16": new Date("2021-10-16T00:00:00.000Z"),
  };
  const currentDate = format(new Date(), "dd");
  const [selectedDate, setSelectedDate] = useState(
    dates[currentDate] ? dates[currentDate] : dates["15"]
  );

  const shedules = [
    {
      id: 1,
      title: "Abertura",
      speaker: {
        name: "NEJ Salvador",
      },
      start_time: "2021-10-15T21:30:00.000Z",
      end_time: "2021-10-15T22:00:00.000Z",
    },
    {
      id: 2,
      title: "Abertura Musical",
      speaker: {
        name: "Maya Fernandes",
      },
      start_time: "2021-10-15T22:00:00.000Z",
      end_time: "2021-10-15T22:15:00.000Z",
    },
    {
      id: 3,
      title: "Salvador enquanto capital de inovação",
      speaker: {
        name:
          "Itala Herta, Hellen Nzinga e Alexandre Darzé",
      },
      description:
        "Mesa redonda de discussão o cenário de inovação empreendedorismo na cidade e quais os impactos sobre o nosso ecossistema.",
      start_time: "2021-10-15T22:20:00.000Z",
      end_time: "2021-10-15T23:20:00.000Z",
    },
    {
      id: 4,
      title: "Hack Redbull",
      // speaker: {
      //   name:
      //     "Itala Herta e Hellen Nzinga",
      // },
      // description:
      //   "Mesa redonda de discussão o cenário de inovação empreendedorismo na cidade e quais os impactos sobre o nosso ecossistema.",
      start_time: "2021-10-15T23:20:00.000Z",
      end_time: "2021-10-15T24:00:00.000Z",
    },
    {
      id: 5,
      title: "Inovação na área da educação",
      speaker: {
        name: "Daniel Neiva e Tata Ribeiro (Aimo Tech)",
      },
      description:
        "Trazer a perspectiva sobre a realidade da educação no Brasil, com enfoque no ambiente universitário.",
      start_time: "2021-10-15T24:00:00.000Z",
      end_time: "2021-10-16T01:00:00.000Z",
    },
    {
      id: 7,
      title: "Empoderamento social e afroempreendedorismo",
      speaker: {
        name: "Joselito Crispim (Grupo Bagunçaço) e Ana Cristina (Vale do Dendê)",
      },
      description: "Mesa redonda sobre empreendedorismo por necessidade, empoderamento social e afroempreendedorismo.",
      start_time: "2021-10-16T12:00:00.000Z",
      end_time: "2021-10-16T13:00:00.000Z",
    },
    {
      id: 14,
      title: "Tecnologia e inovação",
      speaker: {
        name: "Iago Santos (Traz Favela) e David Pires (Cubos)",
      },
      description: "Bate-papo para discussão trazendo o panorama de tecnologia e inovação de Salvador",
      start_time: "2021-10-16T12:00:00.000Z",
      end_time: "2021-10-16T13:00:00.000Z",
    },
    {
      id: 8,
      title: "Sucesso do Cliente",
      speaker: {
        name: "AMBEV e Stone",
      },
      start_time: "2021-10-16T13:00:00.000Z",
      end_time: "2021-10-16T13:40:00.000Z",
    },
    {
      id: 9,
      title: "Cidades inteligentes",
      speaker: {
        name: "Diretor de Inovação Luís Henrique Gaban (Semit) e Rafaela Rodrigues (Parque Tecnológico)",
      },
      description: "Mesa redonda sobre o Plano Diretor de Tecnologias da Cidade Inteligente (PDTCI) e a evolução de Salvador enquanto cidade e falar um pouco sobre espaços em Salvador como Hub Salvador, Colabore e Parque Tecnológico.",
      start_time: "2021-10-16T13:45:00.000Z",
      end_time: "2021-10-16T14:35:00.000Z",
    },
    {
      id: 10,
      title: "LGPD",
      speaker: {
        name: "Escavador",
      },
      start_time: "2021-10-16T14:35:00.000Z",
      end_time: "2021-10-16T15:15:00.000Z",
    },
    {
      id: 11,
      title: "Como a comunicação pode influenciar suas vendas",
      speaker: {
        name: "Impulse",
      },
      start_time: "2021-10-16T14:35:00.000Z",
      end_time: "2021-10-16T15:15:00.000Z",
    },
    {
      id: 12,
      title: "Almoço Musical",
      speaker: {
        name: "Soteropropicanas",
      },
      description: "Nosso momento de almoço será acompanhado de muita música para uma pausa",
      start_time: "2021-10-16T15:25:00.000Z",
      end_time: "2021-10-16T15:55:00.000Z",
    },
    {
      id: 13,
      title: "Cultura Organizacional",
      speaker: {
        name: "Fernanda Lavínia (Intera)",
      },
      description: "Como a cultura de uma empresa pode impactar nos resultados estratégicos",
      start_time: "2021-10-16T16:00:00.000Z",
      end_time: "2021-10-16T16:40:00.000Z",
    },
    {
      id: 14,
      title: "Ingração com o público",
      speaker: {
        name: "NEJ Salvador",
      },
      description: "Sorteio, premiações, interações, jogos",
      start_time: "2021-10-16T16:40:00.000Z",
      end_time: "2021-10-16T17:10:00.000Z",
    },
    {
      id: 15,
      title: "Energia e Sustentabilidade",
      speaker: {
        name: "Saville Alves (SOLOS) e Pedro Rio (Clarke) e Vereador André Fraga", 
      },
      description: "Mesa redonda sobre projetos de energia e sustentabildiade da cidade",
      start_time: "2021-10-16T17:10:00.000Z",
      end_time: "2021-10-16T18:00:00.000Z",
    },
    {
      id: 16,
      title: "Edital de cases",
      speaker: {
        name: "NEJ Salvador",
      },
      description: "Apresentação de cases sobre projetos realizados pelas EJs no ano de 2021 que causaram impacto no nosso ecossistema",
      start_time: "2021-10-16T18:00:00.000Z",
      end_time: "2021-10-16T18:45:00.000Z",
    },
    {
      id: 18,
      title: "Interação com o público",
      description: "",
      start_time: "2021-10-16T18:55:00.000Z",
      end_time: "2021-10-16T19:10:00.000Z",
    },
    {
      id: 19,
      title: "Intervenção Musical",
      speaker: {
        name: "Kauane Messias",
      },
      start_time: "2021-10-16T19:10:00.000Z",
      end_time: "2021-10-16T19:25:00.000Z",
    },
    {
      id: 20,
      title: "Customer Centriciry: o que seria de você sem o cliente?",
      speaker: {
        name: "Marcela Prates e Caio Soares (Konsi)",
      },
      start_time: "2021-10-16T19:25:00.000Z",
      end_time: "2021-10-16T20:05:00.000Z",
    },
    {
      id: 21,
      title: "Palco de Resultados",
      speaker: {
        name: "NEJ Salvador",
      },
      description: "Momento de palco para empresas farol verde, conectadas, impacto e alto impacto e também momento para as filiações oficiais do NEJ Salvador",
      start_time: "2021-10-16T20:10:00.000Z",
      end_time: "2021-10-16T21:10:00.000Z",
    },
    {
      id: 19,
      title: "Encerramento",
      start_time: "2021-10-16T21:10:00.000Z",
      end_time: "2021-10-16T21:30:00.000Z",
    },
  ];

  return (
    <List
      component="nav"
      aria-labelledby="nested-list-subheader"
      subheader={
        <ListSubheader
          css={"color: #fff !important"}
          component="div"
          id="nested-list-subheader"
        >
          Grade do Evento
        </ListSubheader>
      }
      className={classes.root}
    >
      <ListItem
        button
        onClick={() =>
          setSelectedDate(
            selectedDate &&
              selectedDate.toISOString() === dates["15"].toISOString()
              ? undefined
              : dates["15"]
          )
        }
      >
        <ListItemIcon
          css={`
            font-size: 0.8rem;
            color: #fafafa99 !important;
            padding-right: 16px;
            min-width: 24px !important;
          `}
        >
          {!selectedDate ||
          selectedDate.toISOString() !== dates["15"].toISOString() ? (
            <ArrowRightIcon
              css={`
                min-width: 24px !important;
                margin-top: -12px;
              `}
            />
          ) : (
            <ArrowDownIcon
              css={`
                min-width: 24px !important;
                margin-top: -12px;
              `}
            />
          )}
        </ListItemIcon>
        <ListItemText
          primary={`15/10/2021`}
          css={`
            flex-grow: 0 !important;
            margin-right: 18px;
          `}
        />
      </ListItem>
      <Collapse
        in={
          selectedDate &&
          selectedDate.toISOString() === dates["15"].toISOString()
        }
        timeout="auto"
        unmountOnExit
      >
        <List component="div" disablePadding>
          {shedules
            ?.filter(
              (node) =>
                new Date(node.start_time).getDate() == "15" &&
                node.title !== "---"
            )
            .map((node) => (
              <>
                <ListItem
                  button
                  onClick={() =>
                    node.id === currentShedule
                      ? setCurrentShedule(undefined)
                      : setCurrentShedule(node.id)
                  }
                  className={classes.nested}
                >
                  <ListItemIcon
                    css={`
                      font-size: 0.8rem;
                      color: #fafafa99 !important;
                      padding-right: 16px;
                    `}
                  >
                    {format(new Date(node.start_time), "HH:mm")}
                    <br />
                    {format(new Date(node.end_time), "HH:mm")}
                  </ListItemIcon>
                  <ListItemText
                    primary={node.title}
                    secondary={node.speaker && node.speaker.name}
                    className={classes.listItem}
                  />
                  <ListItemSecondaryAction css={"font-size: 12px;"}>
                    <Tooltip
                      title="ADICIONAR NA AGENDA"
                      aria-label="link para calendario do google"
                    >
                      <IconButton
                        edge="end"
                        aria-label="link"
                        target="_blank"
                        rel="noreferrer noopener"
                        href={encodeURI(
                          "https://calendar.google.com/calendar/render?" +
                            Object.entries({
                              action: "TEMPLATE",
                              text: `ESEJ 21: ${node.title}`,
                              dates: `${format(
                                new Date(node.start_time),
                                "yyyyMMdd'T'HHmmss"
                              )}/${format(
                                new Date(node.end_time),
                                "yyyyMMdd'T'HHmmss"
                              )}`,
                              details: `${node.speaker?.name};\n${node.description}`,
                            })
                              .map((entry) => entry.join("="))
                              .join("&")
                        )}
                      >
                        <Calendar color="white" size={18} />
                      </IconButton>
                    </Tooltip>
                  </ListItemSecondaryAction>
                </ListItem>
                <Collapse
                  in={currentShedule === node.id}
                  timeout="auto"
                  unmountOnExit
                >
                  <div
                    css={`
                      padding: 0 96px;
                      font-size: 1rem;
                    `}
                  >
                    {node.description}
                  </div>
                </Collapse>
              </>
            ))}
            
        </List>
      </Collapse>
      <ListItem
        button
        onClick={() =>
          setSelectedDate(
            selectedDate &&
              selectedDate.toISOString() === dates["16"].toISOString()
              ? undefined
              : dates["16"]
          )
        }
      >
        <ListItemIcon
          css={`
            font-size: 0.8rem;
            color: #fafafa99 !important;
            padding-right: 16px;
            min-width: 24px !important;
          `}
        >
          {!selectedDate ||
          selectedDate.toISOString() !== dates["16"].toISOString() ? (
            <ArrowRightIcon
              css={`
                min-width: 24px !important;
                margin-top: -12px;
              `}
            />
          ) : (
            <ArrowDownIcon
              css={`
                min-width: 24px !important;
                margin-top: -12px;
              `}
            />
          )}
        </ListItemIcon>
        <ListItemText
          primary={`16/10/2021`}
          css={`
            flex-grow: 0 !important;
            margin-right: 18px;
          `}
        />
      </ListItem>
      <Collapse
        in={
          selectedDate &&
          selectedDate.toISOString() === dates["16"].toISOString()
        }
        timeout="auto"
        unmountOnExit
      >
        <List component="div" disablePadding>
          {shedules
            ?.filter(
              (node) =>
                new Date(node.start_time).getDate() == "16" &&
                node.title !== "---"
            )
            .map((node) => (
              <>
                <ListItem
                  button
                  onClick={() =>
                    node.id === currentShedule
                      ? setCurrentShedule(undefined)
                      : setCurrentShedule(node.id)
                  }
                  className={classes.nested}
                >
                  <ListItemIcon
                    css={`
                      font-size: 0.8rem;
                      color: #fafafa99 !important;
                      padding-right: 16px;
                    `}
                  >
                    {format(new Date(node.start_time), "HH:mm")}
                    <br />
                    {format(new Date(node.end_time), "HH:mm")}
                  </ListItemIcon>
                  <ListItemText
                    primary={node.title}
                    secondary={node.speaker && node.speaker.name}
                    className={classes.listItem}
                  />
                  <ListItemSecondaryAction css={"font-size: 12px;"}>
                    <Tooltip
                      title="ADICIONAR NA AGENDA"
                      aria-label="link para calendario do google"
                    >
                      <IconButton
                        edge="end"
                        aria-label="link"
                        target="_blank"
                        rel="noreferrer noopener"
                        href={encodeURI(
                          "https://calendar.google.com/calendar/render?" +
                            Object.entries({
                              action: "TEMPLATE",
                              text: `ESEJ 21: ${node.title}`,
                              dates: `${format(
                                new Date(node.start_time),
                                "yyyyMMdd'T'HHmmss"
                              )}/${format(
                                new Date(node.end_time),
                                "yyyyMMdd'T'HHmmss"
                              )}`,
                              details: `${node.speaker?.name};\n${node.description}`,
                            })
                              .map((entry) => entry.join("="))
                              .join("&")
                        )}
                      >
                        <Calendar color="white" size={18} />
                      </IconButton>
                    </Tooltip>
                  </ListItemSecondaryAction>
                </ListItem>
                <Collapse
                  in={currentShedule === node.id}
                  timeout="auto"
                  unmountOnExit
                >
                  <div
                    css={`
                      padding: 0 96px;
                      font-size: 1rem;
                    `}
                  >
                    {node.description}
                  </div>
                </Collapse>
              </>
            ))}
            
        </List>
      </Collapse>
    </List>
  );
}

export default ScheduleList;
