import styled from "styled-components";

export const Container = styled.div`
  position: relative;
  padding-bottom: 38px;
  margin-top: 18px;
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(120px, 1fr));
  grid-gap: 16px 32px;
  align-items: flex-start;

  .speaker {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    text-align: center;
    img,
    .no-photo {
      object-fit: cover;
      width: 80px;
      height: 80px;
      border-radius: 80px;
      /* filter: sepia(1) hue-rotate(180deg); */
      border: 3px solid #333;
      :hover {
        cursor: pointer;
      }
    }
    .no-photo {
      background: #666;
    }
  }
`;
