import React, { useState } from "react";
import "styled-components/macro";
import { Container } from "./styles";

function SpeakerListPreview({ setCurrentSpeaker, speakers, allSpeakers }) {
  const [showAll, setShowAll] = useState(false);
  return (
    <Container>
      {speakers &&
        speakers.map((speaker) => (
          <div key={speaker.id} className="speaker">
            {speaker.picture ? (
              <img
                src={speaker.picture}
                alt={`Foto do palestrante ${speaker.name}`}
                title={`Palestrante: ${speaker.name}`}
                onClick={() => setCurrentSpeaker(speaker)}
              />
            ) : (
              <span
                className="no-photo"
                onClick={() => setCurrentSpeaker(speaker)}
              />
            )}
            <span>{speaker.name}</span>
          </div>
        ))}

      {showAll &&
        allSpeakers
          .filter(({ id }) => !speakers.map((s) => s.id).includes(id))
          .map((speaker) => (
            <div key={speaker.id} className="speaker">
              {speaker.picture ? (
                <img
                  src={speaker.picture.src}
                  alt={`Foto do palestrante ${speaker.name}`}
                  title={`Palestrante: ${speaker.name}`}
                  onClick={() => setCurrentSpeaker(speaker)}
                />
              ) : (
                <span
                  className="no-photo"
                  onClick={() => setCurrentSpeaker(speaker)}
                />
              )}
              <span>{speaker.name}</span>
            </div>
          ))}
      {/* {!showAll ? (
        <GhostButton onClick={() => setShowAll(true)}>
          Ver Todos {">> "}
        </GhostButton>
      ) : (
        <GhostButton onClick={() => setShowAll(false)}>
          Ocultar {"<<"}
        </GhostButton>
      )} */}
    </Container>
  );
}

export default SpeakerListPreview;
