import { Backdrop, Fade, Modal } from "@material-ui/core";
import React from "react";
import "styled-components/macro";
import { useStyles } from "./styles";

function SpeakerModal({ setCurrentSpeaker, currentSpeaker }) {
  const classes = useStyles();
  return (
    <Modal
      aria-labelledby="transition-modal-title"
      aria-describedby="transition-modal-description"
      className={classes.modal}
      open={!!currentSpeaker}
      onClose={() => setCurrentSpeaker()}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 500,
      }}
    >
      <Fade in={!!currentSpeaker}>
        <div className={classes.paper}>
          {currentSpeaker && (
            <img
              src={currentSpeaker.picture}
              alt={`Foto do Palestrante ${currentSpeaker &&
                currentSpeaker.name}`}
              css={
                "width: 140px; height: 140px; background: white; margin-top: 15px; object-fit: cover;"
              }
            />
          )}
          <h2 id="transition-modal-title" css={"margin: 8px 0"}>
            {currentSpeaker && currentSpeaker.name}
          </h2>
          <p id="transition-modal-description">
            {currentSpeaker && currentSpeaker.description}
          </p>
        </div>
      </Fade>
    </Modal>
  );
}

export default SpeakerModal;
