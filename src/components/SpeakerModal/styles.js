import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles((theme) => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    color: "#fff",
    outline: "none",
    padding: 32,
    maxWidth: 800,
    margin: "0 auto",
  },
  paper: {
    backgroundColor: "#00001D",
    border: "2px solid #000010",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    fontSize: "1.1rem",
    color: "#fff",
    outline: "none",
  },
  root: {
    width: "100%",
    background: "#111",
    color: "#fff",
    marginTop: 38,
    // maxWidth: 360
  },
  nested: {
    background: "#111",
    paddingLeft: theme.spacing(4),
  },
  listItem: {
    color: "#FAFAFA !important",
  },
}));
