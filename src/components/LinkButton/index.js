import styled from "styled-components";

const LinkButton = styled.a`
  padding: 8px 12px;
  background: white;
  color: #10101d;
  font-weight: bold;
  font-size: 14px;
  font-family: Agrandir, "Nunito Sans", sans-serif;
  border-radius: 4px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  transition: all 0.2s cubic-bezier(1, 0, 0, 1);
  animation: breathing 1s ease-in-out alternate-reverse infinite;
  :hover {
    animation: none;
    transform: translateX(4px);
    svg {
      margin-left: 18px;
      margin-right: 4px;
    }
  }
  svg {
    transition: all 0.2s ease-in-out;
    margin-left: 4px;
  }

  @keyframes breathing {
    0% {
      transform: scale(1, 1) translateX(0);
    }
    100% {
      transform: scale(1.02, 1.02) translateX(3px);
    }
  }
`;

export default LinkButton;
