import React from "react";

import { Container } from "./styles";

function GhostButton({ children, ...props }) {
  return <Container {...props}>{children}</Container>;
}

export default GhostButton;
