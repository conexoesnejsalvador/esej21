import styled from "styled-components";

export const Container = styled.div`
  background: transparent;
  border: none;
  padding: 4px;
  color: #fafafa;
  text-transform: uppercase;
  right: 0;
  position: absolute;
  bottom: 0;
  font-size: 1rem;
  font-weight: bold;
  :hover {
    cursor: pointer;
  }
`;
