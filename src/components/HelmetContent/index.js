import React from "react";

function HelmetContent() {
  return (
    <>
      <title>Semcomp 2020 - Semana de Computação na UFBA 2020</title>
      <meta
        name="description"
        content="A SEMCOMP UFBA é um evento que visa fomentar o empreendedorismo e movimentar a comunidade de computação."
      />
      <meta
        name="keywords"
        content="Evento, Computação, Tecnologia, Empreendedorismo, Inovação, Salvador, Bahia, Sistemas, Inteligência Artificial, Universidade Federal, Nordeste, Engenharia, Realidade Aumentada, Computação Quântica, Bitcoin, Blockchain"
      />
      <meta name="robots" content="" />
      <meta name="revisit-after" content="7 day" />
      <meta name="language" content="Portuguese" />
      <meta name="generator" content="N/A" />
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <html lang="pt-br" />
    </>
  );
}

export default HelmetContent;
