import React from "react";

import { Badge } from "./styles";

function ComingBadge({ children }) {
  return <Badge>{children ? children : "Em breve!"}</Badge>;
}

export default ComingBadge;
