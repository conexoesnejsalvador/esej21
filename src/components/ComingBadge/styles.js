import styled from "styled-components";

export const Badge = styled.div`
  background: #fff;
  color: #000;
  border-radius: 4px;
  font-size: 0.6rem;
  font-weight: bold;
  padding: 6px;
  box-sizing: border-box;
`;
