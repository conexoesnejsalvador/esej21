import styled from "styled-components";

export const Container = styled.section`
  width: 100%;
`;

export const ContentContainer = styled.div`
  margin: 20px 0 0;
  align-self: center;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-around;
  /* align-items: center; */
  @media (min-width: 800px) {
    /* margin-top: 75px; */
    /* margin-bottom: 75px; */
  }
`;

export const SponsorContainer = styled.a`
  /* width: 50%; */
  margin: 0 12px 20px;
  /* background-color: #c4c4c4; */
  padding: 12px;
  box-sizing: border-box;
  background-size: cover;
  border-radius: 8px;
  display: flex;
  align-items: center;
  justify-content: center;
  min-height: 180px;
  width: 355px;

  /* @media (min-width: 800px) {
    width: 272px;
    height: 272px;
  } */
`;

export const Sponsor = styled.img`
  /* background: red; */
  /* width: 100%; */
  /* height: 100%; */
  width: ${(p) => (p.logoSize ? `${p.logoSize}px` : "180px")};
`;
