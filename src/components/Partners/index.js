import React from "react";
import coLogo from "../../assets/co071.png";
import hubLogo from "../../assets/hub.png";
import boutLogo from "../../assets/boutique.png";
import escarLogo from "../../assets/escariz.png";
import cuboLogo from "../../assets/cubospartner.png";

import {
  Container,
  ContentContainer,
  Sponsor,
  SponsorContainer,
} from "./styles";

const partners = [
  {
    name: "Hub Salvador",
    uri: "https://hubsalvador.com.br/",
    logo: hubLogo,
  },
  {
    name: "CO 071",
    uri: "https://www.co071.com.br/",
    logo: coLogo,
  },
  {
    name: "Boutique Texana",
    uri: "http://boutiquetexana.com.br/",
    logo: boutLogo,
  },
  {
    name: "Escariz",
    uri: "https://www.escariz.com.br/",
    logo: escarLogo,
  },
  {
    name: "Cubos Academy",
    uri: "https://cubos.academy/",
    logo: cuboLogo,
  },
];

function Partners() {
  return (
    <Container className="section">
      <h1>APOIO</h1>

      <ContentContainer>
        {partners.map((partner) => (
          <SponsorContainer
            href={partner.uri}
            target={partner.uri !== "#" && "_blank"}
            title={partner.name}
          >
            <Sponsor logoSize={partner.logoSize} src={partner.logo} />
          </SponsorContainer>
        ))}
      </ContentContainer>
    </Container>
  );
}

export default Partners;
